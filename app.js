//
// app.js
// Dmitry Alexandrov
// B.RF Group
//
const express = require('express')
const app = express()

const cors = require('cors')
app.use(cors()) // разрешить полный доступ к api

const routes = require('./routes')
app.use('/', routes)

// app.use(express.urlencoded({extended: true}))

const morgan = require('morgan')
app.use(morgan('dev'))

// app.use(function (req, res, next) {
//     res.header('Access-Control-Allow-Origin', '*')
//     res.header("Access-Control-Allow-Methods",
//                "GET, POST, PUT, PATCH, DELETE, HEAD, OPTIONS")
//     res.header("Access-Control-Allow-Headers",
//                "Origin, X-Requested-With, Content-Type, Accept, Authorization")
//     next()
// })

const port = 8080
app.listen(port, () => {
    console.log(`Weather API is listening on port: ${port}.`)
})
