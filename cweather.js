//
// cweather.js
// Dmitry Alexandrov
// B.RF Group
//
var fetch = require('node-fetch')

// Retrieve server status
exports.getInfo = ( (req, res) => {  
  res.send({"kind": "success"})
})

// Retrieve weather for city
exports.getWeather = ((req, res) => {
  getWeather(req.params.city, (err, data) => {    
    if (err) {
      console.log(err)
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found ${req.params.city}`
        })
      } else {
        res.status(500).send({
          message: `Error retrieving ${req.params.city}`
        })
      }
    } else res.send(data)
  })
})

async function getWeather(city, result) {
  const url = `http://www.weather-forecast.com/locations/${city}/forecasts/latest`
  console.log(url)
  try {
    const res = await fetch(url)
    const body = await res.text()
    var urlContentArray = await body.split("<span class=\"phrase\">")
    if (urlContentArray.length > 1) {
      let weatherArray = await urlContentArray[1].split("</span>")
      let weather = weatherArray[0]
      weather = await weather.replace(/&deg;/g, "°")
      console.log(weather)
      result(null, { "data": weather } )
    } else {
      result( { "kind": "not_found" }, null)
    }
  } catch (error) {
    console.log(error)
    result( { "kind": "fetch_error" }, null)
  }
}
