//
// routes.js
// Dmitry Alexandrov
// B.RF Group
//
const express = require('express')
const router = express.Router()

const controllerWeather = require('./cweather')

router.get("/", controllerWeather.getInfo)
router.get("/city/:city", controllerWeather.getWeather)

module.exports = router
